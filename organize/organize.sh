#!/bin/bash

# Source folder where the files are located
source_dir="$1"

# Check if a source folder was provided as an argument
if [ -z "$source_dir" ]; then
    echo "Usage: $0 <source_folder>"
    exit 1
fi

# Check if the source folder exists
if [ ! -d "$source_dir" ]; then
    echo "Source folder '$source_dir' does not exist."
    exit 1
fi

# Base directory where files will be organized
base_dir="$source_dir"

# Create destination folder if it doesn't exist
mkdir -p "$base_dir"

# Move files to corresponding folders
move_files() {
    local file_type="$1"
    local destination_dir="$base_dir/$2"

    # Check if files of the specified type exist
    if ls "$source_dir"/*."$file_type" 1> /dev/null 2>&1; then
        # Create destination folder if it doesn't exist
        mkdir -p "$destination_dir"
        # Move files
        mv "$source_dir"/*."$file_type" "$destination_dir"
        echo "$file_type files moved to $destination_dir"
    else
        echo "No $file_type files found in $source_dir"
    fi
}

# Move document files (pdf, docx, txt, etc.)
move_files "pdf" "Documents"
move_files "PDF" "Documents"
move_files "docx" "Documents"
move_files "doc" "Documents"
move_files "txt" "Documents"
move_files "csv" "Documents"
move_files "xlsx" "Documents"
move_files "md" "Documents"
move_files "epub" "Documents"
move_files "xlsm" "Documents"
move_files "xltm" "Documents"
move_files "odt" "Documents"
move_files "xls" "Documents"

# Move video files (mp4, avi, mkv, etc.)
move_files "mp4" "Videos"
move_files "avi" "Videos"
move_files "mkv" "Videos"
move_files "webm" "Videos"

# Move audio files (mp3, wav, flac, etc.)
move_files "wav" "Audio"
move_files "mp3" "Audio"

# Move image files (jpg, jpeg, png, gif, webp, drawio, etc.)
move_files "jpg" "Images"
move_files "jpeg" "Images"
move_files "png" "Images"
move_files "gif" "Images"
move_files "webp" "Images"
move_files "drawio" "Images"
move_files "svg" "Images"

# Move compressed files (zip, tar, etc.)
move_files "zip" "Compressed"
move_files "tar.gz" "Compressed"
move_files "tar.gz.part" "Compressed"
move_files "bz2" "Compressed"
move_files "rar" "Compressed"
move_files "xz" "Compressed"
move_files "ggz" "Compressed"
move_files "7z" "Compressed"

# Move web files (url, etc.)
move_files "url" "Web"

# Move torrents (url, etc.)
move_files "torrent" "Torrents"

# Move programming files (php, sql, ipynb, xml, asm, etc.)
move_files "php" "Code"
move_files "sql" "Code"
move_files "ipynb" "Code"
move_files "xml" "Code"
move_files "asm" "Code"
move_files "rsa" "Code"
move_files "pub" "Code"
move_files "ppk" "Code"
move_files "pacsave" "Code"
move_files "log" "Code"
move_files "html" "Code"
move_files "json" "Code"
move_files "yml" "Code"
move_files "sqlite3" "Code"
move_files "psc" "Code"
move_files "py" "Code"
move_files "hs" "Code"
move_files "css" "Code"
move_files "vim" "Code"
move_files "toml" "Code"
move_files "exe" "Code"
move_files "sh" "Code"
move_files "vimrc" "Code"
move_files "HEX" "Code"
move_files "kdbx" "Code"
move_files "p12" "Code"

echo "Files moved successfully."
