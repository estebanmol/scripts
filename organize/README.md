# File Organizer Script

This Bash script organizes files in a specified directory into categorized subfolders based on their file types. It helps in keeping your files well-organized and easy to find.

## Features

- Automatically organizes files into categories such as Documents, Videos, Audio, Images, Compressed, Web, Torrents, and Code.
- Supports a wide range of file types including pdf, docx, txt, mp4, mp3, jpg, zip, url, torrent, and many more.
- Simple to use: just run the script with the path to the directory containing the files you want to organize.

## Usage

1. Make sure you have Bash installed on your system.
2. Download or clone the script to your local machine.
3. Open a terminal and navigate to the directory where the script is located.
4. Run the script with the following command:


./organize.sh <source_directory>


Replace `<source_directory>` with the path to the directory containing the files you want to organize.
5. Sit back and let the script do the rest! It will create categorized subfolders and move the files accordingly.

## Example

Suppose you have a directory called `~/Downloads` containing various files such as `example.pdf`, `video.mp4`, `music.mp3`, `image.jpg`, `archive.zip`, `website.url`, `program.py`, etc.

Run the script with the following command:

./organize.sh ~/Downloads

After running the script, your directory structure will look like this:

~/Downloads
│ ├── Documents
│ │ └── example.pdf
│ ├── Videos
│ │ └── video.mp4
│ ├── Audio
│ │ └── music.mp3
│ ├── Images
│ │ └── image.jpg
│ ├── Compressed
│ │ └── archive.zip
│ ├── Web
│ │ └── website.url
│ └── Code
│ └── program.py

