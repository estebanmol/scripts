# SSH Connection Monitoring with Email Notifications

This project includes a Bash script and a Python script to monitor SSH connections and authentication attempts on a server. It sends real-time email alerts when specific SSH events are detected.

## Table of Contents

1. [Project Description](#project-description)
2. [Requirements](#requirements)
3. [Configuration](#configuration)
4. [Installation](#installation)
5. [Usage](#usage)
6. [Maintenance](#maintenance)
7. [Credits](#credits)

## Project Description

The project consists of two main components:

1. **`scripts.sh`**: A Bash script that monitors the SSH service log using `journalctl` to detect successful SSH connections, failed authentication attempts, and failed public key authentication attempts. When an event is detected, it calls `send_email.py` to send an email notification.

2. **`send_email.py`**: A Python script that sends emails using the SMTP protocol. This script is invoked by `scripts.sh` with a message describing the detected SSH event.

## Requirements

- **Operating System**: Linux
- **Python**: Version 3.6 or higher
- **Bash**: Compatible with most Bash versions
- **Tools**: `journalctl`, `grep`
- **Python Libraries**: `smtplib`, `email`, `dotenv`

## Configuration

1. **`.env` File**: The `.env` file must be located in the same directory as `send_email.py`. It should contain the following environment variables:

    ```bash
    SMTP_EMAIL=your_email@domain.com
    SMTP_RECIPIENT=recipient@domain.com
    SMTP_SERVER=smtp.server.com
    SMTP_PORT=587
    SMTP_PASSWORD=your_password
    ```

    Replace these values with your actual SMTP configuration details.

## Installation

1. **Clone the Repository**:

    ```bash
    git clone https://gitlab.com/estebanmol/scripts/alerts/ssh-monitor
    cd ssh-monitor
    ```

2. **Install Python Dependencies**:

    Ensure `pip` is installed, then install the `python-dotenv` library:

    ```bash
    pip install python-dotenv
    ```

3. **Create and Configure the `.env` File**:

    Create a `.env` file in the project directory and add the required environment variables as described in the Configuration section.

## Usage

To start monitoring SSH:

1. **Make the `scripts.sh` Script Executable**:

    ```bash
    chmod +x scripts.sh
    ```

2. **Run the Script**:

    ```bash
    ./scripts.sh
    ```

    The script will begin monitoring SSH connections and authentication attempts, sending email alerts as appropriate.

## Maintenance

- **Updating Configuration**: If SMTP credentials or other configuration details change, update the `.env` file.
- **Logs**: You can redirect the output of `scripts.sh` to a log file if you want to keep a record:

    ```bash
    ./scripts.sh > ssh_monitor.log 2>&1 &
    ```

## Credits

This project was developed by [estebanmol](https://gitlab.com/estebanmol). If you have questions or encounter issues, feel free to create an issue on the [project repository](https://gitlab.com/estebanmol/scripts/alerts/ssh-monitor).
