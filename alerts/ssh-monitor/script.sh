#!/bin/bash

# Path to the email sending script
EMAIL_SCRIPT="/PATH/send_email.py"  # Change this to your script's path

# Initialize the timestamp file if it doesn't exist
if [[ ! -f "$TIMESTAMP_FILE" ]]; then
    date --date='2 minutes ago' +"%Y-%m-%d %H:%M:%S" > "$TIMESTAMP_FILE"
fi

# Function to monitor SSH connections
monitor_ssh() {
    local last_check_time="$1"

    # Use journalctl to get logs since the last saved time
    journalctl -u sshd --since "$last_check_time" --until now --output short-iso | while read -r LINE; do
        if [[ $LINE =~ sshd.*Accepted ]]; then
            echo "Detected a successful SSH connection: $LINE"
            python3 "$EMAIL_SCRIPT" "Successful SSH connection: $LINE" || echo "Error sending email for successful SSH connection"
        elif [[ $LINE =~ sshd.*Failed ]]; then
            echo "Detected a failed authentication attempt: $LINE"
            python3 "$EMAIL_SCRIPT" "Failed authentication attempt: $LINE" || echo "Error sending email for failed authentication attempt"
        elif [[ $LINE =~ sshd.*Failed\ publickey ]]; then
            echo "Detected a failed public key authentication attempt: $LINE"
            python3 "$EMAIL_SCRIPT" "Failed public key authentication attempt: $LINE" || echo "Error sending email for failed public key authentication attempt"
        fi
    done
}

# Main loop
while true; do
    # Read the last timestamp from the file
    last_check_time=$(cat "$TIMESTAMP_FILE")

    # Monitor logs since the last recorded time
    monitor_ssh "$last_check_time"

    # Update the timestamp file
    date +"%Y-%m-%d %H:%M:%S" > "$TIMESTAMP_FILE"

    # Wait until the next minute
    sleep $((60 - $(date +%S)))  # Wait until the start of the next minute
done