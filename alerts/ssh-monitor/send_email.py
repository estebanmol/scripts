#!/usr/bin/env python3

import smtplib
from email.mime.text import MIMEText
import sys
from dotenv import load_dotenv
import os

# load file .env
load_dotenv()

def send_email(message):
    sender = os.getenv("SMTP_EMAIL")  # Email sender address
    recipient = os.getenv("SMTP_RECIPIENT")  # Email recipient address
    subject = "Alert: SSH Connection Detected"
    smtp_server = os.getenv("SMTP_SERVER")  # SMTP server
    smtp_port = int(os.getenv("SMTP_PORT"))  # SMTP port
    smtp_user = sender  # SMTP user (email address)
    smtp_password = os.getenv("SMTP_PASSWORD")  # SMTP password from .env

    msg = MIMEText(message)
    msg["Subject"] = subject
    msg["From"] = sender
    msg["To"] = recipient

    try:
        with smtplib.SMTP(smtp_server, smtp_port) as server:
            server.starttls()  # Start encrypted communication
            server.login(smtp_user, smtp_password)  # Authenticate
            server.sendmail(sender, [recipient], msg.as_string())
        print("Email sent successfully.")
    except Exception as e:
        print(f"Error sending email: {e}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("A message is required as an argument.")
        sys.exit(1)
    message = sys.argv[1]
    send_email(message)

